# gdg_cbe_ext

WebExtension Session at Devfest Coimbatore 2019

- MozWebExt book [src](https://github.com/iamVP7/mozwebext_book_src)
- MyExtension [src](https://github.com/iamVP7/MyExtensions)
- MozWoc17 [src](https://github.com/iamVP7/MozWOC17)
- Book to read [leanpub src](https://leanpub.com/mozwebext)