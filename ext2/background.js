chrome.omnibox.onInputChanged.addListener(text_typed);

chrome.omnibox.onInputEntered.addListener(text_entered);

function text_entered(entered_text, disposition) {
    var url = 'https://stackoverflow.com/search?q=';
    url += entered_text;

    chrome.tabs.update({
        url: url
    });

}

function text_typed(typed_text, suggest){
  chrome.omnibox.setDefaultSuggestion({
    description: 'Searching '+typed_text+' in stackoverflow'
  });
}
