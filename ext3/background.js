function menuItemClicked(menu_info_object, tab_object){

	var selectedText = menu_info_object.selectionText || '';
	selectedText = selectedText.replace(/,/g, ' ');
	selectedText = selectedText.replace(/;/g, ' ');
	selectedText = selectedText.replace(/\//g, ' ');
	selectedText = selectedText.replace(/\\/g, ' ');
	selectedText = selectedText.replace(/{/g, ' ');
	selectedText = selectedText.replace(/}/g, ' ');
	selectedText = selectedText.replace(/\n/g, ' ');
	selectedText = selectedText.replace(/\s\s+/g, ' ');

	var totalWords = selectedText.trim().split(' ');
	totalWords = totalWords.filter(word => {
		if (!(word.length === 1 && !word.match(/^[a-zA-Z0-9]+$/)))
			return true;
	});

	var messToPost = 'Total Words:'+ totalWords.length+'\nTotal Characters: '+selectedText.length;

	chrome.notifications.create({
		"type": "basic",
		"title": "Word Count",
		"iconUrl" : "icons/icon-96.png",
		"message": messToPost
	});
}

chrome.contextMenus.create({
	id: "count-me",
	title: 'Word Count',
	contexts: ["all"]
});

chrome.contextMenus.onClicked.addListener(menuItemClicked);
